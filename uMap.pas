unit uMap;

interface

uses Windows, SysUtils, Math;

type
  TXY = record
    x, y: extended;
  end;



const
  TILE_SIZE = 256;


function getWorldXY(latLng: TXY): TXY;
function getTileXY(worldXY: TXY; zoom: byte): TPoint;
function getPixelXY(worldXY: TXY; zoom: byte): TPoint;
function XY(x, y: extended): TXY;

implementation


function XY(x, y: extended): TXY;
begin
  result.x := x;
  result.y := y;
end;

function getWorldXY(latLng: TXY): TXY;
var siny: extended;
begin
  siny := sin(latLng.x * PI() / 180);
  siny := Math.min(Math.max(siny, -0.9999), 0.9999);
  result.x := TILE_SIZE * (0.5 + latLng.y / 360);
  result.y := TILE_SIZE * (0.5 - ln((1 + siny) / (1 - siny)) / (4 * PI()));
end;

function getTileXY(worldXY: TXY; zoom: byte): TPoint;
var scale: int64;
begin
  scale := 1 shl zoom;
  result.X := floor(worldXY.x * scale / TILE_SIZE);
  result.Y := floor(worldXY.y * scale / TILE_SIZE);
end;

function getPixelXY(worldXY: TXY; zoom: byte): TPoint;
var scale: int64;
begin
  scale := 1 shl zoom;
  result.X := floor(worldXY.x * scale) mod TILE_SIZE;
  result.Y := floor(worldXY.y * scale) mod TILE_SIZE;
end;


end.
