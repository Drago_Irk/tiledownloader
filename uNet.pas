unit uNet;

interface

uses Classes, types, sysUtils, forms, dialogs, System.Generics.Collections,
     syncObjs,
     IdHTTP, uMap;

type

  TCounter = class
  private
    fCnt: integer;
    fMax: integer;
    dt1, dt2: TDateTime;
    procedure setCnt(const Value: integer);
    function getCnt: integer;
  public
    constructor create(maxCnt: integer);
    property cnt: integer read getCnt write setCnt;
    procedure incCounter;
  end;

  TWorkerThread = class(TThread)
  private
    fX, fY, fVersion: integer;
    fZoom: byte;
    fID: integer;
    http: TIdHTTP;
    fHaveJob: boolean;
    function getHaveJob: boolean;
    procedure setHaveJob(const Value: boolean);
    function downloadTile(x, y: int64; zoom: byte; version: integer;
      fileName: string): boolean;

  public
    constructor create(id: integer; suspended: boolean);
    destructor destroy; override;

    property haveJob: boolean read getHaveJob write setHaveJob;
    procedure init(x, y: integer; zoom: byte; version: integer);
    procedure execute; override;
  end;

  TWorkerMasterThread = class(TThread)
  private
    fP1, fP2: TXY;
    fZoom: byte;
    fVersion: integer;
    fWorkersCnt: integer;
    fWorkers: TList<TWorkerThread>;
    fReady: boolean;
    function getReady: boolean;

  public
    constructor create(suspended: boolean);
    destructor destroy; override;

    property isReady: boolean read getReady;
    procedure init(workersCnt: integer; p1, p2: TXY; zoom: byte; version: integer);
    function getFreeWorker: TWorkerThread;
    procedure execute; override;

  end;



implementation

uses formMain;

var counter: TCounter;



  { TWorkerThread }

function TWorkerThread.downloadTile(x, y: int64; zoom: byte; version: integer;
  fileName: string): boolean;
var
  mStream: TFileStream;
  s: string;
  ms: TMemoryStream;
begin
  result := false;
  ForceDirectories(ExtractFileDir(fileName));
  //mStream := TFileStream.Create(fileName, fmCreate);
  ms := TMemoryStream.Create;
  try
    s := format('http://khm1.google.com/kh/v=%d&x=%d&y=%d&z=%d&s=Galileo',
               [version, x, y, zoom]);
    try
      http.Get(s, ms{mStream});
    except
      on e: exception do begin
        showMessage(e.Message);
        showMessage(format('�� ������� �������� ����. x=%d, y=%d, zoom=%d',
                   [x, y, zoom]));
      end;
    end;
    if http.ResponseCode = 200 then begin
      result := true;
      counter.incCounter();
    end;
  finally
//    mStream.Free;
    ms.Free;
  end;
end;

constructor TWorkerThread.create(id: integer; suspended: boolean);
begin
  inherited create(suspended);

  fID := id;
  http := TIdHTTP.Create(Application.MainForm);
  http.Request.Username := 'username';
  http.Request.Password := 'password';
  http.Request.UserAgent := 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv'+
                            ': 12.0)Gecko / 20100101 Firefox / 12.0 ';
  HaveJob := false;
end;

destructor TWorkerThread.destroy;
begin
  http.Free;
  inherited;
end;

procedure TWorkerThread.execute;
var rootDir, curDir, fn: string;
begin
  rootDir := ExtractFileDir(Application.ExeName) + '\maps';
  while not terminated do begin
    if haveJob then begin
      curDir := rootDir + format('\%d', [fZoom]);
      fn := curDir + format('\%d-%d.jpg', [fX, fY]);
      downloadTile(fX, fY, fZoom, fVersion, fn);
      haveJob := false;
    end;
    sleep(5);
  end;
end;

function TWorkerThread.getHaveJob: boolean;
begin
  System.TMonitor.enter(self);
  try
    result := fHaveJob;
  finally
    System.TMonitor.Exit(self);
  end;
end;

procedure TWorkerThread.init(x, y: integer; zoom: byte; version: integer);
begin
  fX := x;
  fY := y;
  fZoom := zoom;
  fVersion := version;
  HaveJob := true;
end;

procedure TWorkerThread.setHaveJob(const Value: boolean);
begin
  System.TMonitor.Enter(self);
  try
    fHaveJob := value;
  finally
    System.TMonitor.Exit(self);
  end;
end;

{ TWorkerMasterThread }

constructor TWorkerMasterThread.create(suspended: boolean);
begin
  inherited;
  fWorkers := TList<TWorkerThread>.Create;
  System.TMonitor.Enter(self);
  try
    fReady := false;
  finally
    System.TMonitor.Exit(self);
  end;
end;

destructor TWorkerMasterThread.destroy;
begin
  fWorkers.Free;
  inherited;
end;

procedure TWorkerMasterThread.execute;
var w1, w2: TXY;
    t1, t2, pix1, pix2, pix3, pix4: TPoint;
    w, h: integer;
    x, y: integer;
    worker: TWorkerThread;

begin
  while not terminated do begin
    if isReady then begin
      pix1 := getPixelXY(fP1, fZoom);
      pix2 := getPixelXY(fP2, fZoom);
      pix3 := getPixelXY(XY(fP2.x, fP1.y), fZoom);
      pix4 := getPixelXY(XY(fP1.x, fP2.y), fZoom);
      // ������� �������� � ��������
      w := (t2.X - t1.X)*TILE_SIZE + abs(pix3.X - pix1.X);
      h := (t2.Y - t1.Y)*TILE_SIZE + abs(pix4.Y - pix1.Y);

      w1 := getWorldXY(fP1);
      w2 := getWorldXY(fP2);

      t1 := getTileXY(w1, fZoom);
      t2 := getTileXY(w2, fZoom);

      for y := t1.y to t2.y do begin
        for x := t1.X to t2.X do begin
          worker := nil;
          while worker = nil do begin
            worker := getFreeWorker();
            sleep(5);
          end;

          worker.init(x, y, fZoom, fVersion);
        end;
      end;
      fReady := false;
    end;
    sleep(5);
  end;
end;

function TWorkerMasterThread.getFreeWorker: TWorkerThread;
var t: TWorkerThread;
begin
  result := nil;
  for t in fWorkers do
    if not t.haveJob then begin
      result := t;
      break;
    end;
end;

function TWorkerMasterThread.getReady: boolean;
begin
  System.TMonitor.enter(self);
  try
    result := fReady;
  finally
    System.TMonitor.Exit(self);
  end;
end;

procedure TWorkerMasterThread.init(workersCnt: integer; p1, p2: TXY; zoom: byte;
  version: integer);
var i: integer;
begin
  fWorkersCnt := workersCnt;
  fP1 := p1;
  fP2 := P2;
  fVersion := version;
  fZoom := zoom;

  fWorkers.Clear;
  for i := 1 to workersCnt do
    fWorkers.Add(TWorkerThread.create(i, false));
  System.TMonitor.enter(self);
  try
    fReady := true;
  finally
    System.TMonitor.Exit(self);
  end;
end;

{ TCounter }

constructor TCounter.create(maxCnt: integer);
begin
  inherited create;
  fCnt := 0;
  fMax := maxCnt;
end;

function TCounter.getCnt: integer;
begin
  System.TMonitor.Enter(self);
  try
    result := fCnt;
  finally
    System.TMonitor.Exit(self);
  end;
end;

procedure TCounter.setCnt(const Value: integer);
begin
  System.TMonitor.Enter(self);
  try
    fCnt := value;
  finally
    System.TMonitor.Exit(self);
  end;
end;

procedure TCounter.incCounter;
var h, m, s, ms: word;
begin
  System.TMonitor.Enter(self);
  try
    fCnt := fCnt + 1;
    if fCnt = 1 then dt1 := now;
    if fCnt = fMax then begin
      dt2 := now;
      DecodeTime(dt2-dt1, h, m, s, ms);
   //   Synchronize(procedure begin
        showMessage(format('%d ������ ���� ��������� �� %d ����� %d ����� %d ������ %d �����������', [fMax, h, m, s, ms]));
   //   end);
    end;
  finally
    System.TMonitor.Exit(self);
  end;
end;

initialization
  counter := TCounter.Create(80);

end.
