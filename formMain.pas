unit formMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uMap, Vcl.StdCtrls, IdBaseComponent,
  IdComponent, IdTCPConnection, IdTCPClient, IdHTTP, Vcl.ExtCtrls,
  uNet;

type
  TfrmMain = class(TForm)
    btnStartDownload: TButton;
    edtLat1: TLabeledEdit;
    edtLon1: TLabeledEdit;
    edtLat2: TLabeledEdit;
    edtLon2: TLabeledEdit;
    IdHTTP1: TIdHTTP;
    procedure btnStartDownloadClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

procedure TfrmMain.btnStartDownloadClick(Sender: TObject);
var p1, p2: TXY;
    w1, w2: TXY;
    t1, t2, pix1, pix2, pix3, pix4: TPoint;
    zoom, x, y: Integer;
    fn: string;
    rootDir, curDir: string;
    version: integer;
    w, h: integer;
    wm: TWorkerMasterThread;
    fs: TFormatSettings;

begin
  version := 189;
  fs := TFormatSettings.Create;
  fs.DecimalSeparator := '.';

  rootDir := ExtractFileDir(Application.ExeName) + '\maps';
  ForceDirectories(rootDir);

  p1.x := StrToFloat(edtLat1.Text, fs);
  p1.y := StrToFloat(edtLon1.Text, fs);
  p2.x := StrToFloat(edtLat2.Text, fs);
  p2.y := StrToFloat(edtLon2.Text, fs);

  wm := TWorkerMasterThread.create(false);
  wm.init(10, p1, p2, 18, version);

{
  p1.x := 41.85;
  p1.y := -87.64999999999998;
  w1 := getWorldXY(p1);
  t1 := getTileXY(w1, 8);


  p1.x := String.ToExtended(edtLat1.Text);
  p1.y := String.ToExtended(edtLon1.Text);
  p2.x := String.ToExtended(edtLat2.Text);
  p2.y := String.ToExtended(edtLon2.Text);

  pix1 := getPixelXY(p1, 18);
  pix2 := getPixelXY(p2, 18);
  pix3 := getPixelXY(XY(p2.x, p1.y), 18);
  pix4 := getPixelXY(XY(p1.x, p2.y), 18);
  // ������� �������� � ��������
  w := (t2.X - t1.X)*TILE_SIZE + abs(pix3.X - pix1.X);
  h := (t2.Y - t1.Y)*TILE_SIZE + abs(pix4.Y - pix1.Y);

  w1 := getWorldXY(p1);
  w2 := getWorldXY(p2);

  for zoom := 18 to 18 do begin
    curDir := rootDir + format('\%d', [zoom]);
    ForceDirectories(curDir);

    t1 := getTileXY(w1, zoom);
    t2 := getTileXY(w2, zoom);

    for y := t1.y to t2.y do begin
      for x := t1.X to t2.X do begin
        fn := curDir + format('\%d-%d.jpg', [x, y]);
        if not downloadTile(x, y, zoom, version, fn) then begin
           showMessage(format('�� ������� �������� ����. x=%d, y=%d, zoom=%d',
                 [x, y, zoom]));
        end else begin

        end;
      end;
    end;
  end; // for zoom
  showMessage('Done!');
    {
  showMessage(format('world: (%f; %f)'#13#10+'tile: (%d; %d)',
             [w.X,
              w.Y,
              t.X,
              t.y]));   }
end;

end.
