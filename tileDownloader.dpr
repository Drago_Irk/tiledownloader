program tileDownloader;

uses
  Vcl.Forms,
  formMain in 'formMain.pas' {frmMain},
  uMap in 'uMap.pas',
  uNet in 'uNet.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
